package com.ppb.kelas1.quoteappk1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
class MainActivity : AppCompatActivity() {
    private lateinit var tvAuthor : TextView
    private lateinit var tvQuote : TextView
    private lateinit var parentLayout : ConstraintLayout
    private lateinit var progressBar : ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvAuthor = findViewById(R.id.tv_author)
        tvQuote = findViewById(R.id.tv_quote)
        parentLayout = findViewById(R.id.cl_parent)
        progressBar = findViewById(R.id.progressBar)
        getDataFromApi()
        tvQuote.setOnClickListener {
            getDataFromApi()
        }
        tvAuthor.setOnClickListener {
            getDataFromApi()
        }
        parentLayout.setOnClickListener {
            getDataFromApi()
        }
    }
    private fun getDataFromApi() {
        progressBar.visibility = View.VISIBLE
        val call = ApiClient.apiService.getRandomQuote()
        call.enqueue(object : Callback<Quote> {
            override fun onResponse(call: Call<Quote>, response: Response<Quote>) {
                if (response.isSuccessful) {
                    val quote = response.body()
                    tvQuote.text = quote?.quote
                    tvAuthor.text = quote?.author
                }
                progressBar.visibility  = View.GONE
            }
            override fun onFailure(call: Call<Quote>, t: Throwable) {
            }
        })
    }
}