package com.ppb.kelas1.quoteappk1

data class Quote(
    val author: String,
    val quote: String
)
